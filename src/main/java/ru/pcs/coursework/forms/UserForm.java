package ru.pcs.coursework.forms;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotEmpty;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class UserForm {
    private Integer id;
    private String userName;
    @NotEmpty
    private String password;
    @NotEmpty
    private String repeatPassword;
    private String photo;
    private String about;
    }
