package ru.pcs.coursework.forms;

import lombok.Data;
import org.hibernate.validator.constraints.Length;
import ru.pcs.coursework.models.User;

import javax.validation.constraints.NotEmpty;
import java.util.Date;

@Data
public class MessageForm {
    private int id;

    @NotEmpty
    @Length(max = 280)
    private String text;

    private Date sendTime;
    private User user;
    private Integer parentId;
}
