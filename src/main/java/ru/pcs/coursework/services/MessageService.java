package ru.pcs.coursework.services;

import ru.pcs.coursework.forms.MessageForm;
import ru.pcs.coursework.models.Message;

import java.util.List;

public interface MessageService {
    List<Message> getAll(Integer parentId);
    List<Message> getByUserLogin(String login, Integer parentId);
    void addMessage(MessageForm messageForm);
    void deleteMessage(Integer messageId);
    Message getMessage(Integer id);
    void update(Integer id, MessageForm messageForm);
}
