package ru.pcs.coursework.services;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.pcs.coursework.exceptions.MessageNotFoundException;
import ru.pcs.coursework.forms.MessageForm;
import ru.pcs.coursework.models.Message;
import ru.pcs.coursework.repositories.MessageRepository;

import java.sql.Date;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

@RequiredArgsConstructor
@Service
public class MessageServiceImpl implements MessageService{

    private final MessageRepository messageRepository;

    @Override
    public void addMessage(MessageForm messageForm) {
        Message message = Message.builder()
                .sendTime(new Date(System.currentTimeMillis()))
                .text(messageForm.getText())
                .user(messageForm.getUser())
                .parentId(messageForm.getParentId())
                .countComment(0)
                .build();
        messageRepository.save(message);

        if (messageForm.getParentId()!=0){
            Message messageParent = messageRepository.findMessageById(messageForm.getParentId());
            int count = messageParent.getCountComment();
            messageParent.setCountComment(++count);
            messageRepository.save(messageParent);
        }
    }

    @Override
    public void deleteMessage(Integer messageId) {
        messageRepository.deleteById(messageId);
    }

    @Override
    public Message getMessage(Integer id) {
       return messageRepository.findById(id).orElseThrow(MessageNotFoundException::new);
    }

    @Override
    public void update(Integer id, MessageForm messageForm) {
        Message message = messageRepository.findMessageById(id);
        message.setText(messageForm.getText());
        message.setSendTime(new Date(System.currentTimeMillis()));
        messageRepository.save(message);
    }

    @Override
    public List<Message> getAll(Integer parentId) {
        List<Message> messageList = messageRepository.findMessageByParentId(parentId);
        Collections.sort(messageList, new Comparator<Message>() {
            @Override
            public int compare(Message o1, Message o2) {
                return o2.getId().compareTo(o1.getId());
            }
        });
        return messageList;
    }

    @Override
    public List<Message> getByUserLogin(String login, Integer parentId) {
        List<Message> messageList = messageRepository.findByUserLoginAndParentId(login, parentId);
        Collections.sort(messageList, new Comparator<Message>() {
            @Override
            public int compare(Message o1, Message o2) {
                return o2.getId().compareTo(o1.getId());
            }
        });
        return messageList;
    }


}
