package ru.pcs.coursework.services;

import org.springframework.web.multipart.MultipartFile;
import ru.pcs.coursework.forms.UserForm;
import ru.pcs.coursework.models.User;

import java.security.Principal;

public interface UserService {
    void create(UserForm userForm);
    void updateUser (MultipartFile multipartFile, UserForm userForm, Principal principal);
    User findByLogin(String name);
}
