package ru.pcs.coursework.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import ru.pcs.coursework.forms.UserForm;
import ru.pcs.coursework.models.User;
import ru.pcs.coursework.repositories.UserRepository;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.security.Principal;

@Service
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;
 //   private final BCryptPasswordEncoder passwordEncoder;

    @Autowired
    public UserServiceImpl(UserRepository userRepository){ //BCryptPasswordEncoder passwordEncoder) {
        this.userRepository = userRepository;
  //      this.passwordEncoder = passwordEncoder;
    }

    @Override
    public void create(UserForm userForm) {
        User user = User.builder()
                .login(userForm.getUserName())
                .password(userForm.getPassword())
                .role("USER")
                .photo("")
                .about("")
                .build();
        userRepository.save(user);

    }

    @Override
    public void updateUser(MultipartFile multipartFile, UserForm userForm, Principal principal) {
        String name;
        if (!multipartFile.isEmpty()) {
            try {
                byte[] bytes = multipartFile.getBytes();
                name = multipartFile.getOriginalFilename();
                System.out.println("НАЗВАНИЕ"+name);
                String rootPath = "D:\\JAVA\\ИННОПОЛИС\\coursework\\src\\main\\resources\\static";
                File dir = new File(rootPath + File.separator + "images");
                if (!dir.exists()) {
                    dir.mkdirs();
                }
                User user = userRepository.findByLogin(principal.getName());
                user.setPhoto("\\images\\" + name);
                user.setAbout(userForm.getAbout());
                userRepository.save(user);
                System.out.println("ВАЖНО2!!" + user.getAbout());

                File uploadedFile = new File(dir.getAbsolutePath() + File.separator + name);
                BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(uploadedFile));
                stream.write(bytes);
                stream.flush();
                stream.close();
            } catch (Exception e) {
                System.out.println("failed to upload");
            }
        } else
        {
            User user = userRepository.findByLogin(principal.getName());
            user.setAbout(userForm.getAbout());
            userRepository.save(user);
            System.out.println("failed to upload, becous Empty");
        }
    }

    @Override
    public User findByLogin(String name) {
        return userRepository.findByLogin(name);
    }
}
