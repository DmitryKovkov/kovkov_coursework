package ru.pcs.coursework.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreFilter;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import ru.pcs.coursework.forms.UserForm;
import ru.pcs.coursework.services.UserService;

import javax.validation.Valid;

@Controller
public class LoginController {

    private final UserService userService;

    @Autowired
    public LoginController(UserService userService) {
        this.userService = userService;
    }

    @PreFilter("")
    @GetMapping("/login")
    public String getLoginPage() {
        return "login";
    }

    @PreFilter("")
    @GetMapping("/register")
    public String getRegisterPage() {
        return "register";
    }

   /* @PreFilter("")
    @PostMapping("/login")
    public String auth(Model model) {
        return "login";
    }*/

    @PostMapping("/register")
    public String addUser (@Valid UserForm userForm, BindingResult bindingResult, RedirectAttributes redirectAttributes) {
        if ((bindingResult.hasErrors())||(!userForm.getPassword().equals(userForm.getRepeatPassword()))) {
            redirectAttributes.addFlashAttribute("passwordNotCorrect", "Пароли некорректны!");
            return "redirect:/register";
        }

        userService.create(userForm);
        return "redirect:/login";
    }
}
