package ru.pcs.coursework.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import ru.pcs.coursework.forms.MessageForm;
import ru.pcs.coursework.forms.UserForm;
import ru.pcs.coursework.repositories.UserRepository;
import ru.pcs.coursework.services.MessageService;
import ru.pcs.coursework.services.UserService;

import java.security.Principal;

@Controller
public class ProfileController {

    private final MessageService messageService;
    private final UserService userService;

    @Autowired
    public ProfileController(UserRepository userRepository, MessageService messageService, UserService userService) {
        this.messageService = messageService;
        this.userService = userService;
    }

    @GetMapping("/profile")
    public String getProfilePage(Authentication authentication, Model model, Principal principal) {
        model.addAttribute("user", userService.findByLogin(principal.getName()));
        model.addAttribute("messages", messageService.getByUserLogin(principal.getName(),0));
        return "/profile";
    }

    @PostMapping("/profile")
    public String updateUser(@RequestParam("file") MultipartFile multipartFile, UserForm userForm, Principal principal) {
        userService.updateUser(multipartFile, userForm, principal);
        return "redirect:/profile";
    }

    @PostMapping("/profile/{message-id}/delete")
    public String deleteMessage(@PathVariable("message-id") Integer id) {
        messageService.deleteMessage(id);
        return "redirect:/profile";
    }

    @PostMapping("/profile/{message-id}/update")
    public String updateMessage(@PathVariable("message-id") Integer id, MessageForm messageForm) {
        messageService.update(id, messageForm);
        return "redirect:/profile";
    }

    @GetMapping("/profile/{message-id}")
    public String getMessagePageUpdate (@PathVariable("message-id") Integer id, Authentication authentication, Model model) {
        model.addAttribute("message", messageService.getMessage(id));
        return "updateMessage";
    }
}
