package ru.pcs.coursework.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import ru.pcs.coursework.forms.MessageForm;
import ru.pcs.coursework.models.User;
import ru.pcs.coursework.repositories.UserRepository;
import ru.pcs.coursework.services.MessageService;
import ru.pcs.coursework.services.UserService;

import javax.validation.Valid;
import java.security.Principal;

@Controller
public class MessageController {

    private final MessageService messageService;
    private final UserService userService;

    @Autowired
    public MessageController(MessageService messageService, UserRepository userRepository, UserService userService) {
        this.messageService = messageService;
        this.userService = userService;
    }

    @GetMapping("/home")
    public String getMessagePage(Authentication authentication, Model model, Principal principal) {
        model.addAttribute("messages", messageService.getAll(0));
        model.addAttribute("login", principal.getName());
        return "home";
    }

    @PostMapping("/home")
    public String addMessage(@Valid MessageForm messageForm, BindingResult result,
                                   RedirectAttributes forRedirectModel, Principal principal) {
        if (result.hasErrors()) {
            forRedirectModel.addFlashAttribute("errors", "Заполните твитт!");
            return "redirect:/home";
        }
        User user = userService.findByLogin(principal.getName());
        messageForm.setUser(user);
        messageForm.setParentId(0);
        messageService.addMessage(messageForm);
        return "redirect:/home";
    }

     @PostMapping("/home/{message-id}/comment")
     public String addMessageCom(@PathVariable("message-id") Integer parentId, Authentication authentication, MessageForm messageForm, Principal principal) {
        User user = userService.findByLogin(principal.getName());
        messageForm.setUser(user);
        messageForm.setParentId(parentId);
        messageService.addMessage(messageForm);
      return "redirect:/home/{message-id}";
    }

    @GetMapping("/home/{message-id}")
    public String getMessagePage(@PathVariable("message-id") Integer id, Authentication authentication, Model model) {
        model.addAttribute("message", messageService.getMessage(id));
        model.addAttribute("messagesComment", messageService.getAll(id));
        return "message";
    }


}
