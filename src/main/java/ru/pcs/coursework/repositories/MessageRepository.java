package ru.pcs.coursework.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.pcs.coursework.models.Message;

import java.util.List;

@Repository
public interface MessageRepository extends JpaRepository<Message, Integer> {
    List<Message> findMessageByParentId (Integer parentId);
    Message findMessageById (Integer id);
    List<Message> findByUserLoginAndParentId (String login, Integer parentId);
    Integer countMessageByParentId(Integer id);

}
