package ru.pcs.coursework.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@Table(name = "tw_user")
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @JoinColumn(name = "login")
    private String login;

    private String password;

    @JoinColumn(name = "role")
    private String role;

    @OneToMany(mappedBy = "user")
    private List<Message> messages;

    @JoinColumn(name = "photo")
    private String photo;
    @JoinColumn(name = "about")
    private String about;
}
